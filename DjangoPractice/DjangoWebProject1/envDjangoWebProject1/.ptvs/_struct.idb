�}q (X   docqX  Functions to convert between Python values and C structs.
Python bytes objects are used to hold the data representing the C struct
and also as format strings (explained below) to describe the layout of data
in the C struct.

The optional first format char indicates byte order, size and alignment:
  @: native order, size & alignment (default)
  =: native order, std. size & alignment
  <: little-endian, std. size & alignment
  >: big-endian, std. size & alignment
  !: same as >

The remaining chars indicate types of args and must match exactly;
these can be preceded by a decimal repeat count:
  x: pad byte (no data); c:char; b:signed byte; B:unsigned byte;
  ?: _Bool (requires C99; if not available, char is used instead)
  h:short; H:unsigned short; i:int; I:unsigned int;
  l:long; L:unsigned long; f:float; d:double.
Special cases (preceding decimal count indicates length):
  s:string (array of char); p: pascal string (with count byte).
Special cases (only available in native format):
  n:ssize_t; N:size_t;
  P:an integer type that is wide enough to hold a pointer.
Special case (not in native mode unless 'long long' in platform C):
  q:long long; Q:unsigned long long
Whitespace between formats is ignored.

The variable struct.error is an exception raised on errors.
qX   membersq}q(X	   pack_intoq}q(X   kindqX   functionqX   valueq	}q
(hX  pack_into(fmt, buffer, offset, v1, v2, ...)

Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.qX	   overloadsq]q(}q(X   argsq(}qX   nameqX   fmtqs}qhX   bufferqs}qhX   offsetqs}qhX   v1qs}qhX   v2qs}q(X
   arg_formatqX   *qhhutqhX�   Pack the values v1, v2, ... according to the format string fmt and write
the packed bytes into the writable buffer buf starting at offset.  Note
that the offset is a required argument.  See help(struct) for more
on format strings.qu}q (X   argsq!(}q"(X   typeq#]q$X   __builtin__q%X   strq&�q'aX   nameq(X   fmtq)u}q*(h#]q+X   arrayq,X   arrayq-�q.ah(X   bufferq/u}q0(h#]q1h%X   intq2�q3ah(X   offsetq4u}q5(h#]q6h%X   tupleq7�q8aX
   arg_formatq9hh(X   argsq:utq;X   ret_typeq<]q=h%X   NoneTypeq>�q?aueuuX   __spec__q@}qA(hX   dataqBh	}qCX   typeqD]qEX   _frozen_importlibqFX
   ModuleSpecqG�qHasuX   _clearcacheqI}qJ(hhh	}qK(hX   Clear the internal cache.qLh]qM(}qN(h}qO(hhhhu}qP(hX   **qQhX   kwargsqRu�qShX   Clear the internal cache.qTu}qU(h!)h<]qVh?aueuuX   __doc__qW}qX(hhBh	}qYhD]qZ(X   builtinsq[X   strq\�q]h?esuX   unpack_fromq^}q_(hhh	}q`(hX�   unpack_from(fmt, buffer, offset=0) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.qah]qb(}qc(h}qdhX   fmtqes}qfhX   bufferqgs}qh(X   default_valueqiX   0qjhX   offsetqku�qlX   ret_typeqm]qnX    qoho�qpahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)
for more on format strings.qqu}qr(h!}qs(h#]qth'ah(X   fmtquu}qv(h#]qwh.ah(X   bufferqxu}qy(h#]qzh3ah(X   offsetq{X   default_valueq|hju�q}h<]q~h8au}q(h!}q�(h#]q�h'ah(X   fmtq�u}q�(h#]q�h'ah(X   bufferq�u}q�(h#]q�h3ah(X   offsetq�h|hju�q�h<]q�h8au}q�(h!}q�(h#]q�h'ah(X   fmtq�u}q�(h#]q�h%X   bufferq��q�ah(X   bufferq�u}q�(h#]q�h3ah(X   offsetq�h|hju�q�h<]q�h8aueuuX   BuiltinImporterq�}q�(hhDh	}q�(hX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    q�X	   is_hiddenq��h}q�(X   __subclasshook__q�}q�(hhh	}q�(hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
q�uauuX   __str__q�}q�(hX   methodq�h	}q�(hX   Return str(self).q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   Return str(self).q�uauuX   __le__q�}q�(hh�h	}q�(hX   Return self<=value.q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   Return self<=value.q�uauuX
   __reduce__q�}q�(hh�h	}q�(hX   helper for pickleq�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   helper for pickleq�uauuX
   is_packageq�}q�(hhh	}q�(hX4   Return False as built-in modules are never packages.q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX4   Return False as built-in modules are never packages.q�uauuX   __setattr__q�}q�(hh�h	}q�(hX%   Implement setattr(self, name, value).q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX%   Implement setattr(self, name, value).q�uauuX
   __format__q�}q�(hh�h	}q�(hX   default object formatterq�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   default object formatterq�uauuX	   __class__q�}q�(hX   typerefq�h	]q�h[X   typeq�q�auX   __ne__q�}q�(hh�h	}q�(hX   Return self!=value.q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   Return self!=value.q�uauuX   __delattr__q�}q�(hh�h	}q�(hX   Implement delattr(self, name).q�h]q�}q�(h}q�(hhhhu}q�(hhQhhRu�q�hX   Implement delattr(self, name).q�uauuX	   find_specr   }r  (hhBh	}r  hD]r  h[X   methodr  �r  asuX   __ge__r  }r  (hh�h	}r  (hX   Return self>=value.r	  h]r
  }r  (h}r  (hhhhu}r  (hhQhhRu�r  hX   Return self>=value.r  uauuX   module_reprr  }r  (hhh	}r  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r  h]r  }r  (h}r  (hhhhu}r  (hhQhhRu�r  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r  uauuX   load_moduler  }r  (hhh	}r  (hX   Load a built-in module.r  h]r  }r  (h}r   (hhhhu}r!  (hhQhhRu�r"  hX   Load a built-in module.r#  uauuhW}r$  (hhBh	}r%  hD]r&  h]asuX
   get_sourcer'  }r(  (hhh	}r)  (hX8   Return None as built-in modules do not have source code.r*  h]r+  }r,  (h}r-  (hhhhu}r.  (hhQhhRu�r/  hX8   Return None as built-in modules do not have source code.r0  uauuX   __init__r1  }r2  (hh�h	}r3  (hX>   Initialize self.  See help(type(self)) for accurate signature.r4  h]r5  }r6  (h}r7  (hhhhu}r8  (hhQhhRu�r9  hX>   Initialize self.  See help(type(self)) for accurate signature.r:  uauuX   __hash__r;  }r<  (hh�h	}r=  (hX   Return hash(self).r>  h]r?  }r@  (h}rA  (hhhhu}rB  (hhQhhRu�rC  hX   Return hash(self).rD  uauuX   __repr__rE  }rF  (hh�h	}rG  (hX   Return repr(self).rH  h]rI  }rJ  (h}rK  (hhhhu}rL  (hhQhhRu�rM  hX   Return repr(self).rN  uauuX
   __module__rO  }rP  (hhBh	}rQ  hD]rR  h]asuX   __eq__rS  }rT  (hh�h	}rU  (hX   Return self==value.rV  h]rW  }rX  (h}rY  (hhhhu}rZ  (hhQhhRu�r[  hX   Return self==value.r\  uauuX   get_coder]  }r^  (hhh	}r_  (hX9   Return None as built-in modules do not have code objects.r`  h]ra  }rb  (h}rc  (hhhhu}rd  (hhQhhRu�re  hX9   Return None as built-in modules do not have code objects.rf  uauuX
   __sizeof__rg  }rh  (hh�h	}ri  (hX6   __sizeof__() -> int
size of object in memory, in bytesrj  h]rk  }rl  (h}rm  (hD]rn  h[X   objectro  �rp  ahX   selfrq  u�rr  hm]rs  h[X   intrt  �ru  ahX"   size of object in memory, in bytesrv  uauuX   __new__rw  }rx  (hhh	}ry  (hXG   Create and return a new object.  See help(type) for accurate signature.rz  h]r{  }r|  (h}r}  (hhhhu}r~  (hhQhhRu�r  hXG   Create and return a new object.  See help(type) for accurate signature.r�  uauuX   __weakref__r�  }r�  (hX   propertyr�  h	}r�  (hX2   list of weak references to the object (if defined)r�  hD]r�  h[X   objectr�  �r�  auuX   __lt__r�  }r�  (hh�h	}r�  (hX   Return self<value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Return self<value.r�  uauuX   __dir__r�  }r�  (hh�h	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (hD]r�  jp  ahjq  u�r�  hm]r�  h[X   listr�  �r�  ahX   default dir() implementationr�  uauuX   find_moduler�  }r�  (hhh	}r�  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauuX   __reduce_ex__r�  }r�  (hh�h	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   helper for pickler�  uauuX   __dict__r�  }r�  (hhBh	}r�  hD]r�  h[X   mappingproxyr�  �r�  asuX   __gt__r�  }r�  (hh�h	}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Return self>value.r�  uauuuX   basesr�  ]r�  j�  aX   mror�  ]r�  (hFh��r�  j�  euuX   calcsizer�  }r�  (hhh	}r�  (hX`   calcsize(fmt) -> integer

Return size in bytes of the struct described by the format string fmt.r�  h]r�  (}r�  (h}r�  hX   fmtr�  s�r�  hm]r�  ju  ahXF   Return size in bytes of the struct described by the format string fmt.r�  u}r�  (h!}r�  (h#]r�  h'ah(X   fmtr�  u�r�  h<]r�  h3aueuuX
   __loader__r�  }r�  (hh�h	]r�  j�  auX   __name__r�  }r�  (hhBh	}r�  hD]r�  (h]h'esuX   Structr�  }r�  (hh�h	]r�  h[X   Structr�  �r�  auX   unpackr�  }r�  (hhh	}r�  (hX�   unpack(fmt, buffer) -> (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.r�  h]r�  (}r�  (h}r�  hX   fmtr�  s}r�  hX   bufferr�  s�r�  hm]r�  hpahX�   (v1, v2, ...)

Return a tuple containing values unpacked according to the format string
fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more
on format strings.r�  u}r�  (h!}r�  (h#]r�  h'ah(X   fmtr�  u}r�  (h#]r�  h'ah(X   stringr�  u�r�  h<]r�  h8au}r�  (h!}r�  (h#]r�  h'ah(X   fmtr�  u}r   (h#]r  h.ah(X   bufferr  u�r  h<]r  h8au}r  (h!}r  (h#]r  h'ah(X   fmtr  u}r	  (h#]r
  h�ah(X   bufferr  u�r  h<]r  h8aueuuX   iter_unpackr  }r  (hhh	}r  (hX  iter_unpack(fmt, buffer) -> iterator(v1, v2, ...)

Return an iterator yielding tuples unpacked from the given bytes
source according to the format string, like a repeated invocation of
unpack_from().  Requires that the bytes length be a multiple of the
format struct size.r  h]r  }r  (h}r  hX   fmtr  s}r  hX   bufferr  s�r  hm]r  hoX   iteratorr  �r  ahX�   (v1, v2, ...)

Return an iterator yielding tuples unpacked from the given bytes
source according to the format string, like a repeated invocation of
unpack_from().  Requires that the bytes length be a multiple of the
format struct size.r  uauuX   __package__r  }r  (hhBh	}r  hD]r   (h]h?esuX   errorr!  }r"  (hhDh	}r#  (hhoh}r$  (h�}r%  (hh�h	}r&  (hX   Return str(self).r'  h]r(  (}r)  (h}r*  (hhhhu}r+  (hhQhhRu�r,  hX   Return str(self).r-  u}r.  (h!}r/  (h#]r0  h%X   objectr1  �r2  ah(X   selfr3  u�r4  h<]r5  h'aueuuh�}r6  (hhh	}r7  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r8  h]r9  }r:  (h}r;  (hhhhu}r<  (hhQhhRu�r=  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r>  uauuX   __setstate__r?  }r@  (hh�h	}rA  (hX.   __setstate__(self: BaseException, state: dict)rB  h]rC  }rD  (h!}rE  (h#]rF  X
   exceptionsrG  X   BaseExceptionrH  �rI  ah(X   selfrJ  u}rK  (h#]rL  h%X   dictrM  �rN  ah(X   staterO  u�rP  h<]rQ  h?auauuh�}rR  (hh�h	}rS  (hX   Return self<=value.rT  h]rU  }rV  (h}rW  (hhhhu}rX  (hhQhhRu�rY  hX   Return self<=value.rZ  uauuX   with_tracebackr[  }r\  (hh�h	}r]  (hXQ   Exception.with_traceback(tb) --
    set self.__traceback__ to tb and return self.r^  h]r_  }r`  (h}ra  (hD]rb  jp  ahjq  u}rc  hX   tbrd  s�re  hX-   set self.__traceback__ to tb and return self.rf  uauuh�}rg  (hh�h	}rh  (hX   helper for pickleri  h]rj  }rk  (h!}rl  (h#]rm  jI  ah(jJ  u�rn  h<]ro  j2  auauuj�  }rp  (hhBh	}rq  hD]rr  (j�  h%X	   dictproxyrs  �rt  esuh�}ru  (hh�h	}rv  (hX%   Implement setattr(self, name, value).rw  h]rx  (}ry  (h}rz  (hhhhu}r{  (hhQhhRu�r|  hX%   Implement setattr(self, name, value).r}  u}r~  (h!}r  (h#]r�  j2  ah(X   selfr�  u}r�  (h#]r�  h'ah(X   namer�  u}r�  (h#]r�  j2  ah(X   valuer�  u�r�  h<]r�  h?aueuuh�}r�  (hh�h	}r�  (hX   default object formatterr�  h]r�  (}r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   default object formatterr�  u}r�  (h!}r�  (h#]r�  j2  ah(X   selfr�  u}r�  (h#]r�  h'ah(X
   formatSpecr�  u�r�  h<]r�  h'aueuuh�}r�  (hh�h	]r�  h�auh�}r�  (hh�h	}r�  (hX   Return self!=value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Return self!=value.r�  uauuh�}r�  (hh�h	}r�  (hX   Implement delattr(self, name).r�  h]r�  (}r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Implement delattr(self, name).r�  u}r�  (h!}r�  (h#]r�  j2  ah(X   selfr�  u}r�  (h#]r�  h'ah(X   namer�  u�r�  h<]r�  h?aueuuh}r�  (hj�  h	}r�  hD]r�  (j�  j2  esuj  }r�  (hh�h	}r�  (hX   Return self>=value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Return self>=value.r�  uauuj�  }r�  (hh�h	}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   Return self>value.r�  uauuj�  }r�  (hh�h	}r�  (hX   helper for pickler�  h]r�  (}r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX   helper for pickler�  u}r�  (h!}r�  (h#]r�  jI  ah(jJ  u}r�  (h#]r�  h3ah(X   protocolr�  u�r�  h<]r�  j2  aueuuX   __suppress_context__r�  }r�  (hj�  h	}r�  hD]r�  j�  asujg  }r�  (hh�h	}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr�  h]r�  (}r�  (h}r�  (hD]r�  jp  ahjq  u�r�  hm]r�  ju  ahX"   size of object in memory, in bytesr�  u}r�  (h!}r�  (h#]r�  j2  ah(X   selfr�  u�r�  h<]r�  h3aueuuhW}r�  (hhBh	}r�  hD]r�  (h[X   NoneTyper�  �r�  h'esuj1  }r�  (hh�h	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h]r�  (}r�  (h}r�  (hhhhu}r�  (hhQhhRu�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r   u}r  (h!}r  (h#]r  jI  ah(jJ  u}r  (h#]r  h8ah9hh(X   argsr  u�r  h<]r  h?aueuuj�  }r	  (hh�h	}r
  (hX   Return self<value.r  h]r  }r  (h}r  (hhhhu}r  (hhQhhRu�r  hX   Return self<value.r  uauujE  }r  (hh�h	}r  (hX   Return repr(self).r  h]r  (}r  (h}r  (hhhhu}r  (hhQhhRu�r  hX   Return repr(self).r  u}r  (h!}r  (h#]r  jI  ah(jJ  u�r  h<]r  h'aueuujO  }r   (hhBh	}r!  hD]r"  (h]h'esujS  }r#  (hh�h	}r$  (hX   Return self==value.r%  h]r&  }r'  (h}r(  (hhhhu}r)  (hhQhhRu�r*  hX   Return self==value.r+  uauuX   __context__r,  }r-  (hj�  h	}r.  (hX   exception contextr/  hD]r0  j�  auuX   __traceback__r1  }r2  (hj�  h	}r3  hD]r4  j�  asujw  }r5  (hhh	}r6  (hXG   Create and return a new object.  See help(type) for accurate signature.r7  h]r8  (}r9  (h}r:  (hhhhu}r;  (hhQhhRu�r<  hXG   Create and return a new object.  See help(type) for accurate signature.r=  u}r>  (h!}r?  (h#]r@  h%X   typerA  �rB  ah(X   clsrC  u}rD  (h#]rE  jN  ah9X   **rF  h(X   kwArgsrG  u}rH  (h#]rI  h8ah9hh(X   argsrJ  u�rK  h<]rL  j2  au}rM  (h!}rN  (h#]rO  jB  ah(X   clsrP  u}rQ  (h#]rR  h8ah9hh(X   argsrS  u�rT  h<]rU  j2  aueuuj�  }rV  (hj�  h	}rW  (hX2   list of weak references to the object (if defined)rX  hD]rY  j�  auuj�  }rZ  (hh�h	}r[  (hX.   __dir__() -> list
default dir() implementationr\  h]r]  }r^  (h}r_  (hD]r`  jp  ahjq  u�ra  hm]rb  j�  ahX   default dir() implementationrc  uauuX	   __cause__rd  }re  (hj�  h	}rf  (hX   exception causerg  hD]rh  j�  auuj;  }ri  (hh�h	}rj  (hX   Return hash(self).rk  h]rl  (}rm  (h}rn  (hhhhu}ro  (hhQhhRu�rp  hX   Return hash(self).rq  u}rr  (h!}rs  (h#]rt  j2  ah(X   selfru  u�rv  h<]rw  h3aueuuuj�  ]rx  h[X	   Exceptionry  �rz  aj�  ]r{  (X   structr|  X   errorr}  �r~  jz  h[X   BaseExceptionr  �r�  j�  euuX   packr�  }r�  (hhh	}r�  (hX�   pack(fmt, v1, v2, ...) -> bytes

Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.r�  h]r�  (}r�  (h(}r�  hX   fmtr�  s}r�  hX   v1r�  s}r�  hX   v2r�  s}r�  (hhhhutr�  hm]r�  h[X   bytesr�  �r�  ahX�   Return a bytes object containing the values v1, v2, ... packed according
to the format string fmt.  See help(struct) for more on format strings.r�  u}r�  (h!}r�  (h#]r�  h'ah(X   fmtr�  u}r�  (h#]r�  h8ah9hh(X   valuesr�  u�r�  h<]r�  h'aueuuuu.