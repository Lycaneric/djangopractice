�}q (X   docqX  This module defines an object type which can efficiently represent
an array of basic values: characters, integers, floating point
numbers.  Arrays are sequence types and behave very much like lists,
except that the type of objects stored in them is constrained.
qX   membersq}q(X   __spec__q}q(X   kindqX   dataqX   valueq	}q
X   typeq]qX   _frozen_importlibqX
   ModuleSpecq�qasuX   arrayq}q(hhh	}q(hX5	  array(typecode [, initializer]) -> array

Return a new array whose items are restricted by typecode, and
initialized from the optional initializer value, which must be a list,
string or iterable over elements of the appropriate type.

Arrays represent basic values and behave very much like lists, except
the type of objects stored in them is constrained. The type is specified
at object creation time by using a type code, which is a single character.
The following type codes are defined:

    Type code   C Type             Minimum size in bytes 
    'b'         signed integer     1 
    'B'         unsigned integer   1 
    'u'         Unicode character  2 (see note) 
    'h'         signed integer     2 
    'H'         unsigned integer   2 
    'i'         signed integer     2 
    'I'         unsigned integer   2 
    'l'         signed integer     4 
    'L'         unsigned integer   4 
    'q'         signed integer     8 (see note) 
    'Q'         unsigned integer   8 (see note) 
    'f'         floating point     4 
    'd'         floating point     8 

NOTE: The 'u' typecode corresponds to Python's unicode character. On 
narrow builds this is 2-bytes on wide builds this is 4-bytes.

NOTE: The 'q' and 'Q' type codes are only available if the platform 
C compiler used to build Python supports 'long long', or, on Windows, 
'__int64'.

Methods:

append() -- append a new item to the end of the array
buffer_info() -- return information giving the current memory info
byteswap() -- byteswap all the items of the array
count() -- return number of occurrences of an object
extend() -- extend array by appending multiple elements from an iterable
fromfile() -- read items from a file object
fromlist() -- append items from the list
frombytes() -- append items from the string
index() -- return index of first occurrence of an object
insert() -- insert a new item into the array at a provided position
pop() -- remove and return item (default last)
remove() -- remove first occurrence of an object
reverse() -- reverse the order of the items in the array
tofile() -- write all items to a file object
tolist() -- return the array converted to an ordinary list
tobytes() -- return the array converted to a string

Attributes:

typecode -- the typecode character used to create the array
itemsize -- the length in bytes of one array item
qh}q(X   __str__q}q(hX   methodqh	}q(hX   Return str(self).qX	   overloadsq]q(}q(X   argsq}q(X
   arg_formatqX   *q X   nameq!hu}q"(hX   **q#h!X   kwargsq$u�q%hX   Return str(self).q&u}q'(X   argsq(}q)(X   typeq*]q+X   __builtin__q,X   objectq-�q.aX   nameq/X   oq0u�q1X   ret_typeq2]q3h,X   strq4�q5aueuuX	   frombytesq6}q7(hhh	}q8(hX�   frombytes(bytestring)

Appends items from the string, interpreting it as an array of machine
values, as if it had been read from a file using the fromfile() method).q9h]q:}q;(h}q<(h]q=X   builtinsq>X   objectq?�q@ah!X   selfqAu}qBh!X
   bytestringqCs�qDhX�   Appends items from the string, interpreting it as an array of machine
values, as if it had been read from a file using the fromfile() method).qEuauuX   __le__qF}qG(hhh	}qH(hX   Return self<=value.qIh]qJ(}qK(h}qL(hh h!hu}qM(hh#h!h$u�qNhX   Return self<=value.qOu}qP(h(}qQ(h*]qRX   arrayqSX   arrayqT�qUah/X   selfqVu}qW(h*]qXh.ah/X   otherqYu�qZh2]q[h.aueuuX
   __reduce__q\}q](hhh	}q^(hX   helper for pickleq_h]q`(}qa(h}qb(hh h!hu}qc(hh#h!h$u�qdhX   helper for pickleqeu}qf(h(}qg(h*]qhhUah/X   selfqiu�qjh2]qkh,X   tupleql�qmaueuuX   __iadd__qn}qo(hhh	}qp(hX   Implement self+=value.qqh]qr(}qs(h}qt(hh h!hu}qu(hh#h!h$u�qvhX   Implement self+=value.qwu}qx(h(}qy(h*]qzhUah/hiu}q{(h*]q|hUah/X   otherq}u�q~h2]qhUaueuuX   __imul__q�}q�(hhh	}q�(hX   Implement self*=value.q�h]q�(}q�(h}q�(hh h!hu}q�(hh#h!h$u�q�hX   Implement self*=value.q�u}q�(h(}q�(h*]q�hUah/hiu}q�(h*]q�h,X   intq��q�ah/X   valueq�u�q�h2]q�hUaueuuX   __copy__q�}q�(hhh	}q�(hX)   copy(array)

 Return a copy of the array.q�h]q�(}q�(h}q�(hh h!hu}q�(hh#h!h$u�q�hX)   copy(array)

 Return a copy of the array.q�u}q�(h(}q�(h*]q�hUah/hiu�q�h2]q�hUaueuuX   __delattr__q�}q�(hhh	}q�(hX   Implement delattr(self, name).q�h]q�(}q�(h}q�(hh h!hu}q�(hh#h!h$u�q�hX   Implement delattr(self, name).q�u}q�(h(}q�(h*]q�h.ah/X   selfq�u}q�(h*]q�h5ah/X   nameq�u�q�h2]q�h,X   NoneTypeq��q�aueuuX   fromunicodeq�}q�(hhh	}q�(hX�   fromunicode(ustr)

Extends this array with data from the unicode string ustr.
The array must be a unicode type array; otherwise a ValueError
is raised.  Use array.frombytes(ustr.encode(...)) to
append Unicode data to an array of some other type.q�h]q�(}q�(h}q�(h]q�h@ah!hAu}q�h!X   ustrq�s�q�hX�   Extends this array with data from the unicode string ustr.
The array must be a unicode type array; otherwise a ValueError
is raised.  Use array.frombytes(ustr.encode(...)) to
append Unicode data to an array of some other type.q�u}q�(h(}q�(h*]q�hUah/hiu}q�(h*]q�h5ah/X   sq�u�q�h2]q�h�aueuuX
   __format__q�}q�(hhh	}q�(hX   default object formatterq�h]q�(}q�(h}q�(hh h!hu}q�(hh#h!h$u�q�hX   default object formatterq�u}q�(h(}q�(h*]q�h.ah/X   selfq�u}q�(h*]q�h5ah/X
   formatSpecq�u�q�h2]q�h5aueuuX   __len__q�}q�(hhh	}q�(hX   Return len(self).q�h]q�(}q�(h}q�(hh h!hu}q�(hh#h!h$u�q�hX   Return len(self).q�u}q�(h(}q�(h*]q�hUah/hiu�q�h2]q�h�aueuuX   byteswapq�}q�(hhh	}q�(hX�   byteswap()

Byteswap all items of the array.  If the items in the array are not 1, 2,
4, or 8 bytes in size, RuntimeError is raised.q�h]q�(}q�(h}q�(h]q�h@ah!hAu�q�hXx   Byteswap all items of the array.  If the items in the array are not 1, 2,
4, or 8 bytes in size, RuntimeError is raised.q�u}q�(h(}q�(h*]q�hUah/hiu�q�h2]q�h�aueuuX   __add__q�}q�(hhh	}q�(hX   Return self+value.r   h]r  (}r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX   Return self+value.r  u}r  (h(}r  (h*]r	  hUah/X   selfr
  u}r  (h*]r  hUah/X   otherr  u�r  h2]r  hUaueuuX   __rmul__r  }r  (hhh	}r  (hX   Return self*value.r  h]r  (}r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX   Return self*value.r  u}r  (h(}r  (h*]r  h,X   longr  �r  ah/X   valuer  u}r   (h*]r!  hUah/X   arrayr"  u�r#  h2]r$  hUau}r%  (h(}r&  (h*]r'  h�ah/X   valuer(  u}r)  (h*]r*  hUah/X   arrayr+  u�r,  h2]r-  hUaueuuX   __gt__r.  }r/  (hhh	}r0  (hX   Return self>value.r1  h]r2  (}r3  (h}r4  (hh h!hu}r5  (hh#h!h$u�r6  hX   Return self>value.r7  u}r8  (h(}r9  (h*]r:  hUah/X   selfr;  u}r<  (h*]r=  h.ah/X   otherr>  u�r?  h2]r@  h.aueuuX	   tounicoderA  }rB  (hhh	}rC  (hX�   tounicode() -> unicode

Convert the array to a unicode string.  The array must be
a unicode type array; otherwise a ValueError is raised.  Use
array.tobytes().decode() to obtain a unicode string from
an array of some other type.rD  h]rE  (}rF  (h}rG  (h]rH  h@ah!hAu�rI  X   ret_typerJ  ]rK  h>X   strrL  �rM  ahX�   Convert the array to a unicode string.  The array must be
a unicode type array; otherwise a ValueError is raised.  Use
array.tobytes().decode() to obtain a unicode string from
an array of some other type.rN  u}rO  (h(}rP  (h*]rQ  hUah/hiu�rR  h2]rS  h5aueuuX   fromfilerT  }rU  (hhh	}rV  (hX^   fromfile(f, n)

Read n objects from the file object f and append them to the end of the
array.rW  h]rX  (}rY  (h}rZ  (h]r[  h@ah!hAu}r\  h!X   fr]  s}r^  h!X   nr_  s�r`  hXN   Read n objects from the file object f and append them to the end of the
array.ra  u}rb  (h(}rc  (h*]rd  hUah/hiu}re  (h*]rf  h,X   filerg  �rh  ah/j]  u}ri  (h*]rj  h�ah/j_  u�rk  h2]rl  h�aueuuX   reverserm  }rn  (hhh	}ro  (hX7   reverse()

Reverse the order of the items in the array.rp  h]rq  (}rr  (h}rs  (h]rt  h@ah!hAu�ru  hX,   Reverse the order of the items in the array.rv  u}rw  (h(}rx  (h*]ry  hUah/hiu�rz  h2]r{  h�aueuuX   __doc__r|  }r}  (hhh	}r~  h]r  (h>X   strr�  �r�  h5esuX   __init__r�  }r�  (hhh	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  u}r�  (h(}r�  (h*]r�  h.ah/X   selfr�  u}r�  (h*]r�  h,X   dictr�  �r�  aX
   arg_formatr�  X   **r�  h/X   kwargsr�  u}r�  (h*]r�  hmaj�  h h/X   argsr�  u�r�  h2]r�  h�au}r�  (h(}r�  (h*]r�  h.ah/X   selfr�  u}r�  (h*]r�  hmaj�  h h/X   argsr�  u�r�  h2]r�  h�au}r�  (h(}r�  (h*]r�  h.ah/X   selfr�  u�r�  h2]r�  h�aueuuX   buffer_infor�  }r�  (hhh	}r�  (hX  buffer_info() -> (address, length)

Return a tuple (address, length) giving the current memory address and
the length in items of the buffer used to hold array's contents
The length should be multiplied by the itemsize attribute to calculate
the buffer length in bytes.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu�r�  jJ  ]r�  X    r�  j�  �r�  ahX�   (address, length)

Return a tuple (address, length) giving the current memory address and
the length in items of the buffer used to hold array's contents
The length should be multiplied by the itemsize attribute to calculate
the buffer length in bytes.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu�r�  h2]r�  hmaueuuX   __eq__r�  }r�  (hhh	}r�  (hX   Return self==value.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self==value.r�  u}r�  (h(}r�  (h*]r�  h.ah/X   yr�  u}r�  (h*]r�  hUah/X   xr�  u�r�  h2]r�  h.au}r�  (h(}r�  (h*]r�  hUah/j�  u}r�  (h*]r�  h.ah/j�  u�r�  h2]r�  h.au}r�  (h(}r�  (h*]r�  hUah/j�  u}r�  (h*]r�  hUah/j�  u�r�  h2]r�  h,X   boolr�  �r�  aueuuX   __setitem__r�  }r�  (hhh	}r�  (hX   Set self[key] to value.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Set self[key] to value.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h,X   slicer�  �r�  ah/X   indexr�  u}r�  (h*]r�  h.ah/X   valuer�  u�r�  h2]r�  h�au}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h�ah/X   indexr�  u}r�  (h*]r�  h.ah/X   valuer�  u�r   h2]r  h�aueuuX
   fromstringr  }r  (hhh	}r  (hX�   fromstring(string)

Appends items from the string, interpreting it as an array of machine
values, as if it had been read from a file using the fromfile() method).

This method is deprecated. Use frombytes instead.r  h]r  (}r  (h}r  (h]r	  h@ah!hAu}r
  h!X   stringr  s�r  hX�   Appends items from the string, interpreting it as an array of machine
values, as if it had been read from a file using the fromfile() method).

This method is deprecated. Use frombytes instead.r  u}r  (h(}r  (h*]r  hUah/hiu}r  (h*]r  h,X   bufferr  �r  ah/X   bufr  u�r  h2]r  h�au}r  (h(}r  (h*]r  hUah/hiu}r  (h*]r  h5ah/h�u�r  h2]r  h�au}r  (h(}r   (h*]r!  hUah/hiu}r"  (h*]r#  h,X   bytesr$  �r%  ah/X   br&  u�r'  h2]r(  h�aueuuX   appendr)  }r*  (hhh	}r+  (hX6   append(x)

Append new value x to the end of the array.r,  h]r-  (}r.  (h}r/  (h]r0  h@ah!hAu}r1  h!j�  s�r2  hX+   Append new value x to the end of the array.r3  u}r4  (h(}r5  (h*]r6  hUah/hiu}r7  (h*]r8  h.ah/X   iterabler9  u�r:  h2]r;  h�aueuuX   __deepcopy__r<  }r=  (hhh	}r>  (hX)   copy(array)

 Return a copy of the array.r?  h]r@  (}rA  (h}rB  (hh h!hu}rC  (hh#h!h$u�rD  hX)   copy(array)

 Return a copy of the array.rE  u}rF  (h(}rG  (h*]rH  hUah/hiu�rI  h2]rJ  hUaueuuX   __hash__rK  }rL  (hhh	}rM  h]rN  (h>X   NoneTyperO  �rP  h�esuX   __subclasshook__rQ  }rR  (hX   functionrS  h	}rT  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rU  h]rV  }rW  (h}rX  (hh h!hu}rY  (hh#h!h$u�rZ  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r[  uauuX   __setattr__r\  }r]  (hhh	}r^  (hX%   Implement setattr(self, name, value).r_  h]r`  (}ra  (h}rb  (hh h!hu}rc  (hh#h!h$u�rd  hX%   Implement setattr(self, name, value).re  u}rf  (h(}rg  (h*]rh  h.ah/X   selfri  u}rj  (h*]rk  h5ah/X   namerl  u}rm  (h*]rn  h.ah/X   valuero  u�rp  h2]rq  h�aueuuX   tobytesrr  }rs  (hhh	}rt  (hXh   tobytes() -> bytes

Convert the array to an array of machine values and return the bytes
representation.ru  h]rv  }rw  (h}rx  (h]ry  h@ah!hAu�rz  jJ  ]r{  h>X   bytesr|  �r}  ahXT   Convert the array to an array of machine values and return the bytes
representation.r~  uauuX
   __sizeof__r  }r�  (hhh	}r�  (hX;   __sizeof__() -> int

Size of the array in memory, in bytes.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu�r�  jJ  ]r�  h>X   intr�  �r�  ahX&   Size of the array in memory, in bytes.r�  u}r�  (h(}r�  (h*]r�  h.ah/X   selfr�  u�r�  h2]r�  h�aueuuX   indexr�  }r�  (hhh	}r�  (hX=   index(x)

Return index of first occurrence of x in the array.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu}r�  h!j�  s�r�  hX3   Return index of first occurrence of x in the array.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h.ah/j�  u�r�  h2]r�  h�aueuuX   remover�  }r�  (hhh	}r�  (hX9   remove(x)

Remove the first occurrence of x in the array.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu}r�  h!j�  s�r�  hX.   Remove the first occurrence of x in the array.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h.ah/X   valuer�  u�r�  h2]r�  h�aueuuX   extendr�  }r�  (hhh	}r�  (hXA   extend(array or iterable)

 Append items to the end of the array.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu}r�  (h]r�  j�  X   orr�  �r�  ah!X   iterabler�  u�r�  hX%   Append items to the end of the array.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h.ah/X   iterabler�  u�r�  h2]r�  h�aueuuX   __dir__r�  }r�  (hhh	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (h]r�  h@ah!hAu�r�  jJ  ]r�  h>X   listr�  �r�  ahX   default dir() implementationr�  uauuX	   __class__r�  }r�  (hX   typerefr�  h	]r�  h>X   typer�  �r�  auX   __ne__r�  }r�  (hhh	}r�  (hX   Return self!=value.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self!=value.r�  u}r�  (h(}r�  (h*]r�  h.ah/j�  u}r�  (h*]r�  hUah/j�  u�r�  h2]r�  h.au}r�  (h(}r�  (h*]r�  hUah/j�  u}r�  (h*]r�  h.ah/j�  u�r�  h2]r�  h.au}r�  (h(}r�  (h*]r�  hUah/j�  u}r�  (h*]r�  hUah/j�  u�r�  h2]r�  j�  aueuuX   __delitem__r   }r  (hhh	}r  (hX   Delete self[key].r  h]r  (}r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX   Delete self[key].r	  u}r
  (h(}r  (h*]r  hUah/hiu}r  (h*]r  j�  ah/X   slicer  u�r  h2]r  h�au}r  (h(}r  (h*]r  hUah/hiu}r  (h*]r  h�ah/X   indexr  u�r  h2]r  h�aueuuX   typecoder  }r  (hX   propertyr  h	}r  (hX/   the typecode character used to create the arrayr  h]r  (h>X   objectr   �r!  h5euuX   __iter__r"  }r#  (hhh	}r$  (hX   Implement iter(self).r%  h]r&  }r'  (h}r(  (hh h!hu}r)  (hh#h!h$u�r*  hX   Implement iter(self).r+  uauuX   __getitem__r,  }r-  (hhh	}r.  (hX   Return self[key].r/  h]r0  (}r1  (h}r2  (hh h!hu}r3  (hh#h!h$u�r4  hX   Return self[key].r5  u}r6  (h(}r7  (h*]r8  hUah/hiu}r9  (h*]r:  j�  ah/X   indexr;  u�r<  h2]r=  h.au}r>  (h(}r?  (h*]r@  hUah/hiu}rA  (h*]rB  h�ah/X   indexrC  u�rD  h2]rE  h.aueuuX   insertrF  }rG  (hhh	}rH  (hXB   insert(i,x)

Insert a new item x into the array before position i.rI  h]rJ  (}rK  (h}rL  (h]rM  h@ah!hAu}rN  h!X   irO  s}rP  h!j�  s�rQ  hX5   Insert a new item x into the array before position i.rR  u}rS  (h(}rT  (h*]rU  hUah/hiu}rV  (h*]rW  h�ah/jO  u}rX  (h*]rY  h.ah/j�  u�rZ  h2]r[  h�aueuuX   __mul__r\  }r]  (hhh	}r^  (hX   Return self*value.nr_  h]r`  (}ra  (h}rb  (hh h!hu}rc  (hh#h!h$u�rd  hX   Return self*value.nre  u}rf  (h(}rg  (h*]rh  hUah/X   arrayri  u}rj  (h*]rk  j  ah/X   valuerl  u�rm  h2]rn  hUau}ro  (h(}rp  (h*]rq  hUah/X   arrayrr  u}rs  (h*]rt  h�ah/X   valueru  u�rv  h2]rw  hUaueuuX   __contains__rx  }ry  (hhh	}rz  (hX   Return key in self.r{  h]r|  (}r}  (h}r~  (hh h!hu}r  (hh#h!h$u�r�  hX   Return key in self.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h.ah/X   valuer�  u�r�  h2]r�  j�  aueuuX   itemsizer�  }r�  (hj  h	}r�  (hX%   the size, in bytes, of one array itemr�  h]r�  (j!  h�euuX   tostringr�  }r�  (hhh	}r�  (hX�   tostring() -> bytes

Convert the array to an array of machine values and return the bytes
representation.

This method is deprecated. Use tobytes instead.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu�r�  jJ  ]r�  j}  ahX�   Convert the array to an array of machine values and return the bytes
representation.

This method is deprecated. Use tobytes instead.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu�r�  h2]r�  h5aueuuX   __ge__r�  }r�  (hhh	}r�  (hX   Return self>=value.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self>=value.r�  u}r�  (h(}r�  (h*]r�  hUah/X   selfr�  u}r�  (h*]r�  h.ah/X   otherr�  u�r�  h2]r�  h.aueuuX   __lt__r�  }r�  (hhh	}r�  (hX   Return self<value.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self<value.r�  u}r�  (h(}r�  (h*]r�  hUah/X   selfr�  u}r�  (h*]r�  h.ah/X   otherr�  u�r�  h2]r�  h.aueuuX   __repr__r�  }r�  (hhh	}r�  (hX   Return repr(self).r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return repr(self).r�  u}r�  (h(}r�  (h*]r�  hUah/hiu�r�  h2]r�  h5aueuuX   fromlistr�  }r�  (hhh	}r�  (hX0   fromlist(list)

Append items to array from list.r�  h]r�  (}r�  (h}r�  (h]r�  h@ah!hAu}r�  h!X   listr�  s�r�  hX    Append items to array from list.r�  u}r�  (h(}r�  (h*]r�  hUah/hiu}r�  (h*]r�  h.ah/X   iterabler�  u�r�  h2]r�  h�aueuuX   __new__r�  }r�  (hjS  h	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h]r�  (}r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hXG   Create and return a new object.  See help(type) for accurate signature.r�  u}r�  (h(}r�  (h*]r�  h,X   typer�  �r�  ah/X   typer�  u}r�  (h*]r�  hmaj�  h h/X   argsr�  u�r�  h2]r�  h.au}r�  (h(}r�  (h*]r�  j�  ah/X   typer   u}r  (h*]r  j�  aj�  j�  h/X   kwargsr  u}r  (h*]r  hmaj�  h h/X   argsr  u�r  h2]r  h.au}r	  (h(}r
  (h*]r  j�  ah/X   typer  u}r  (h*]r  j�  aj�  j�  h/X   kwargsr  u�r  h2]r  h.aueuuX   tolistr  }r  (hhh	}r  (hXH   tolist() -> list

Convert array to an ordinary list with the same items.r  h]r  (}r  (h}r  (h]r  h@ah!hAu�r  jJ  ]r  j�  ahX6   Convert array to an ordinary list with the same items.r  u}r  (h(}r  (h*]r  hUah/hiu�r   h2]r!  h,X   listr"  �r#  aueuuX   countr$  }r%  (hhh	}r&  (hX9   count(x)

Return number of occurrences of x in the array.r'  h]r(  (}r)  (h}r*  (h]r+  h@ah!hAu}r,  h!j�  s�r-  hX/   Return number of occurrences of x in the array.r.  u}r/  (h(}r0  (h*]r1  hUah/hiu}r2  (h*]r3  h.ah/j�  u�r4  h2]r5  h�aueuuX   tofiler6  }r7  (hhh	}r8  (hXD   tofile(f)

Write all items (as machine values) to the file object f.r9  h]r:  (}r;  (h}r<  (h]r=  h@ah!hAu}r>  h!j]  s�r?  hX9   Write all items (as machine values) to the file object f.r@  u}rA  (h(}rB  (h*]rC  hUah/hiu}rD  (h*]rE  jh  ah/j]  u�rF  h2]rG  h�aueuuX   __reduce_ex__rH  }rI  (hhh	}rJ  (hX&   Return state information for pickling.rK  h]rL  (}rM  (h}rN  (hh h!hu}rO  (hh#h!h$u�rP  hX&   Return state information for pickling.rQ  u}rR  (h(}rS  (h*]rT  hUah/hiu�rU  h2]rV  hmau}rW  (h(}rX  (h*]rY  hUah/hiu}rZ  (h*]r[  h�ah/X   versionr\  u�r]  h2]r^  hmaueuuX   popr_  }r`  (hhh	}ra  (hXQ   pop([i])

Return the i-th element and delete it from the array. i defaults to -1.rb  h]rc  (}rd  (h}re  (h]rf  h@ah!hAu}rg  (X   default_valuerh  X   Noneri  h!jO  u�rj  hXG   Return the i-th element and delete it from the array. i defaults to -1.rk  u}rl  (h(}rm  (h*]rn  hUah/hiu}ro  (h*]rp  h�ah/jO  u�rq  h2]rr  h.au}rs  (h(}rt  (h*]ru  hUah/hiu�rv  h2]rw  h.aueuuuX   basesrx  ]ry  j!  aX   mrorz  ]r{  (X   arrayr|  X   arrayr}  �r~  j!  euuX   __package__r  }r�  (hhh	}r�  h]r�  (j�  h�esuj|  }r�  (hhh	}r�  h]r�  (j�  h5esuX	   ArrayTyper�  }r�  (hj�  h	]r�  j~  auX   BuiltinImporterr�  }r�  (hhh	}r�  (hX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �h}r�  (jQ  }r�  (hjS  h	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauuh}r�  (hhh	}r�  (hX   Return str(self).r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return str(self).r�  uauuhF}r�  (hhh	}r�  (hX   Return self<=value.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self<=value.r�  uauuh\}r�  (hhh	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   helper for pickler�  uauuX
   is_packager�  }r�  (hjS  h	}r�  (hX4   Return False as built-in modules are never packages.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX4   Return False as built-in modules are never packages.r�  uauuj\  }r�  (hhh	}r�  (hX%   Implement setattr(self, name, value).r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX%   Implement setattr(self, name, value).r�  uauuh�}r�  (hhh	}r�  (hX   default object formatterr�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   default object formatterr�  uauuj�  }r�  (hj�  h	]r�  j�  auj�  }r�  (hhh	}r�  (hX   Return self!=value.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self!=value.r�  uauuh�}r�  (hhh	}r�  (hX   Implement delattr(self, name).r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Implement delattr(self, name).r�  uauuX	   find_specr�  }r�  (hhh	}r�  h]r�  h>X   methodr�  �r�  asuj�  }r�  (hhh	}r�  (hX   Return self>=value.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self>=value.r�  uauuX   module_reprr�  }r�  (hjS  h	}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  uauuX   load_moduler�  }r�  (hjS  h	}r�  (hX   Load a built-in module.r�  h]r   }r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX   Load a built-in module.r  uauuj|  }r  (hhh	}r  h]r  j�  asuX
   get_sourcer	  }r
  (hjS  h	}r  (hX8   Return None as built-in modules do not have source code.r  h]r  }r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX8   Return None as built-in modules do not have source code.r  uauuj�  }r  (hhh	}r  (hX>   Initialize self.  See help(type(self)) for accurate signature.r  h]r  }r  (h}r  (hh h!hu}r  (hh#h!h$u�r  hX>   Initialize self.  See help(type(self)) for accurate signature.r  uauujK  }r  (hhh	}r  (hX   Return hash(self).r  h]r  }r   (h}r!  (hh h!hu}r"  (hh#h!h$u�r#  hX   Return hash(self).r$  uauuj�  }r%  (hhh	}r&  (hX   Return repr(self).r'  h]r(  }r)  (h}r*  (hh h!hu}r+  (hh#h!h$u�r,  hX   Return repr(self).r-  uauuX
   __module__r.  }r/  (hhh	}r0  h]r1  j�  asuj�  }r2  (hhh	}r3  (hX   Return self==value.r4  h]r5  }r6  (h}r7  (hh h!hu}r8  (hh#h!h$u�r9  hX   Return self==value.r:  uauuX   get_coder;  }r<  (hjS  h	}r=  (hX9   Return None as built-in modules do not have code objects.r>  h]r?  }r@  (h}rA  (hh h!hu}rB  (hh#h!h$u�rC  hX9   Return None as built-in modules do not have code objects.rD  uauuj  }rE  (hhh	}rF  (hX6   __sizeof__() -> int
size of object in memory, in bytesrG  h]rH  }rI  (h}rJ  (h]rK  h@ah!hAu�rL  jJ  ]rM  j�  ahX"   size of object in memory, in bytesrN  uauuj�  }rO  (hjS  h	}rP  (hXG   Create and return a new object.  See help(type) for accurate signature.rQ  h]rR  }rS  (h}rT  (hh h!hu}rU  (hh#h!h$u�rV  hXG   Create and return a new object.  See help(type) for accurate signature.rW  uauuX   __weakref__rX  }rY  (hj  h	}rZ  (hX2   list of weak references to the object (if defined)r[  h]r\  j!  auuj�  }r]  (hhh	}r^  (hX   Return self<value.r_  h]r`  }ra  (h}rb  (hh h!hu}rc  (hh#h!h$u�rd  hX   Return self<value.re  uauuj�  }rf  (hhh	}rg  (hX.   __dir__() -> list
default dir() implementationrh  h]ri  }rj  (h}rk  (h]rl  h@ah!hAu�rm  jJ  ]rn  j�  ahX   default dir() implementationro  uauuX   find_modulerp  }rq  (hjS  h	}rr  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        rs  h]rt  }ru  (h}rv  (hh h!hu}rw  (hh#h!h$u�rx  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        ry  uauujH  }rz  (hhh	}r{  (hX   helper for pickler|  h]r}  }r~  (h}r  (hh h!hu}r�  (hh#h!h$u�r�  hX   helper for pickler�  uauuX   __dict__r�  }r�  (hhh	}r�  h]r�  h>X   mappingproxyr�  �r�  asuj.  }r�  (hhh	}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX   Return self>value.r�  uauuujx  ]r�  j!  ajz  ]r�  (hj�  �r�  j!  euuX   _array_reconstructorr�  }r�  (hjS  h	}r�  (hX$   Internal. Used for pickling support.r�  h]r�  }r�  (h}r�  (hh h!hu}r�  (hh#h!h$u�r�  hX$   Internal. Used for pickling support.r�  uauuX	   typecodesr�  }r�  (hhh	}r�  h]r�  (j�  h5esuX
   __loader__r�  }r�  (hj�  h	]r�  j�  auX   __name__r�  }r�  (hhh	}r�  h]r�  (j�  h5esuuu.